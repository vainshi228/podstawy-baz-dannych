use Szpital;
go
drop table if exists KartyMiedyczne;
go
create table KartyMiedyczne(
    numer_karty int not null identity(1,1),
    id_pacienta varchar(5) not null,

    data_przyjecia date not null,
    numer_oddzialu int not null,
    data_zwolnienia date not null,

    objawy text not null,
    zapisane_przez varchar(5) not null,

    diagnoza text not null,
    zadiagnozowane_przez varchar(5) not null,

    constraint pk_numer_karty_medycznej primary key (numer_karty),
    constraint fk_id_pacienta_na_karcie_miedycznej
                           foreign key (id_pacienta)
                           references Klienci(id_pacjenta)
)
go
drop table if exists Preciptions;
go
create table Preciptions (
    id_preciptions int not null identity(1, 1),
    id_karty_miedycznej int not null,

    nazwa varchar(30) not null,
    opis text,
    dawkowanie varchar(20),
    metoda_podania varchar(30),
    cena_jednastkowa money not null,
    liczba_na_dzien int,
    data_rozpoczecia date,
    data_zokonczenia date,


    constraint pk_preciption primary key (id_preciptions),
    constraint fk_preciption_numer_karty foreign key (id_karty_miedycznej)
                         references KartyMiedyczne(numer_karty),
)
go
use master
go
use master;
go
drop database if exists Szpital;
go
create database Szpital;
go
use Szpital;
go
drop table if exists Klienci;
go
create table Klienci(
    id_pacjenta varchar(5) not null,
    imie varchar(30) not null,
    piec varchar(30) not null,
    data_urodzenia date not null,
    adres text not null,
    ubiezpieczenie varchar(10) not null,
    nazwa_firmy varchar(40) not null,
    adres_firmy text not null,
    telefon varchar(13) not null,
    przedstawiciel varchar(50) not null,
    data_rejstracji date not null default getdate(),
    nazwisko varchar(30) not null,

    constraint pk_client primary key (id_pacjenta),
);
go
use master;
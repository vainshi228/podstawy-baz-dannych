USE PIZZA;

-- 1.1
SELECT count(pizza) as Total_pizzas FROM MENU;

-- 1.2
SELECT count(DISTINCT country) as Origins FROM MENU;

-- 1.3
SELECT min(price) as Cheapest_Italian FROM MENU WHERE country = 'Italy';

-- 1.4
SELECT round(sum(price), 2) as Margherita_Vegetarian FROM MENU WHERE pizza IN ('margherita', 'vegetarian');

-- 1.5
SELECT min(price) as MIN_PRICE, max(price) as MAX_PRICE, round(avg(price), 4) as AVG_PRICE FROM MENU;

-- 1.6
SELECT count(base) as 'no of wheat mix' FROM MENU WHERE base = 'wholemeal';

-- 1.7
SELECT count(price) as no_origin FROM MENU WHERE country IS NULL;

-- 1.8
SELECT round(avg(price) * 50 * 0.3, 2) as profit FROM MENU; -- chyba oki już

-- 2.1
SELECT i.ingredient, i.type FROM ITEMS i
    INNER JOIN RECIPES r ON r.ingredient = i.ingredient
    WHERE r.pizza = 'margherita';

-- 2.2
SELECT r.ingredient, r.pizza FROM RECIPES r
    INNER JOIN ITEMS i ON i.ingredient = r.ingredient
    WHERE i.type = "fish";

-- 2.3
SELECT r.ingredient, r.pizza FROM RECIPES r
    RIGHT JOIN ITEMS i ON i.ingredient = r.ingredient
    WHERE i.type = 'meat' ORDER BY r.pizza;

-- 2.4
SELECT m1.pizza FROM MENU m1
    INNER JOIN MENU m2 ON m1.country = m2.country
    WHERE m2.pizza = 'siciliano' AND m1.pizza <> 'siciliano';

-- 2.5
SELECT m1.pizza FROM MENU m1
    INNER JOIN MENU m2 ON m1.price > m2.price
    WHERE m2.pizza = 'quattro stagioni' AND m1.price > m2.price;

-- 2.6
SELECT i.ingredient, r.pizza FROM RECIPES r
    RIGHT JOIN ITEMS i ON i.ingredient = r.ingredient
    WHERE i.type = "fish";

-- 2.7
SELECT DISTINCT i.type FROM ITEMS i
    INNER JOIN RECIPES r ON r.ingredient = i.ingredient
    INNER JOIN MENU m ON m.pizza = r.pizza
    WHERE m.country IN ('Canada', 'Mexico', 'U.S.');

-- 2.8
SELECT m.pizza FROM MENU m
    INNER JOIN RECIPES r ON r.pizza = m.pizza
    INNER JOIN ITEMS i ON i.ingredient = r.ingredient
    WHERE m.base = "wholemeal" AND i.type = "fruit";

USE master;
GO

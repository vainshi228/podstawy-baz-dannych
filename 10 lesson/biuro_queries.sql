USE biuro;
GO

-- 1
SELECT k.imie, k.nazwisko FROM klienci k
	WHERE k.telefon IS NULL
	ORDER BY LEN(k.nazwisko) DESC;
GO

-- 2
SELECT k.imie, k.nazwisko FROM klienci k
	INNER JOIN wynajecia w ON k.klientnr = w.klientnr
	WHERE w.nieruchomoscnr
	IN (SELECT w2.nieruchomoscnr
		FROM wizyty w2
		WHERE w2.klientnr = k.klientnr)
	GROUP BY k.klientnr, k.imie, k.nazwisko ;
GO


-- 3
SELECT k.imie, k.nazwisko FROM klienci k
	LEFT JOIN wynajecia w ON k.klientnr = w.klientnr
	WHERE w.czynsz > k.max_czynsz
	GROUP BY k.klientnr, k.imie, k.nazwisko ;
GO


-- Dla każdego klienta wyświetl jego imię, nazwisko oraz liczbę nieruchomości, które oglądał przed wynajęciem pierwszej z nich.
-- Uwzględnij tylko tych klientów, którzy oglądali więcej niż jedną nieruchomość.
-- 4
SELECT k.imie, k.nazwisko, COUNT(w.nieruchomoscnr) AS liczba_nieruchomosci FROM klienci k
	LEFT JOIN wizyty w ON k.klientnr = w.klientnr
    WHERE w.data_wizyty < (
        SELECT TOP 1 w2.od_kiedy FROM wynajecia w2
        WHERE w2.klientnr = k.klientnr
        ORDER BY w2.od_kiedy DESC
    )
	GROUP BY k.klientnr, k.imie, k.nazwisko
    HAVING COUNT(w.nieruchomoscnr) > 1
GO

-- Dla każdego klienta wyświetl jego imię, nazwisko oraz numery wszystkich nieruchomości,
-- których czynsz jest niższy niż zadeklarowana przez niego maksymalna kwota.
-- 5
SELECT k.imie, k.nazwisko, n.nieruchomoscnr
    FROM klienci k, nieruchomosci n
    WHERE k.max_czynsz > n.czynsz;
GO

-- Wyświetl imię oraz nazwisko właściciela, który posiada najwięcej nieruchomości.
-- Uwzględnij, że rezultatów może być wiele.
-- 6
SELECT w.imie, w.nazwisko
    FROM wlasciciele w
    JOIN nieruchomosci n on w.wlascicielnr = n.wlascicielnr
    GROUP BY w.wlascicielnr, w.imie, w.nazwisko
    HAVING COUNT(n.nieruchomoscnr) = (
        SELECT TOP 1 COUNT(n2.wlascicielnr)
            FROM nieruchomosci n2
            GROUP BY n2.wlascicielnr
            ORDER BY COUNT(n2.wlascicielnr) DESC
        );
GO

-- Dla każdego właściciela wyświetl jego imię, nazwisko
-- oraz numery wszystkich nieruchomości,które posiada.
-- 7
SELECT w.imie, w.nazwisko, n.nieruchomoscnr
    FROM wlasciciele w
    JOIN nieruchomosci n on w.wlascicielnr = n.wlascicielnr;
GO

-- Dla każdego biura wyświetl liczbę zatrudnionych w nim kobiet oraz mężczyzn.
-- Uwzględnij wszystkie biura. Zastosuj aliasy K oraz M.
-- 8
SELECT b.biuronr, (
    SELECT COUNT(p1.plec)
        FROM personel p1
        WHERE p1.plec = 'K' AND p1.biuronr = b.biuronr) AS K,
       (SELECT COUNT(p1.plec)
        FROM personel p1
        WHERE p1.plec = 'M' AND p1.biuronr = b.biuronr) AS M
    FROM biura b;
GO

-- Dla każdego miasta wyświetl liczbę zatrudnionych w nim kobiet oraz mężczyzn.
-- Uwzględnij wszystkie miasta. Zastosuj aliasy K oraz M.
-- 9
SELECT DISTINCT b.miasto, (
    SELECT COUNT(p1.plec)
        FROM personel p1
        JOIN biura b2 on p1.biuronr = b2.biuronr
        WHERE p1.plec = 'K' AND b.miasto = b2.miasto) AS K,
       (SELECT COUNT(p1.plec)
        FROM personel p1
        JOIN biura b3 on p1.biuronr = b3.biuronr
        WHERE p1.plec = 'M' AND b.miasto = b3.miasto) AS M
    FROM biura b;
GO

-- Dla każdego stanowiska wyświetl liczbę zatrudnionych na nim kobiet oraz mężczyzn.
-- Uwzględnij wszystkie stanowiska. Zastosuj aliasy K oraz M.
-- 10
SELECT DISTINCT p.stanowisko, (
    SELECT COUNT(p1.plec)
        FROM personel p1
        WHERE p1.plec = 'K' AND p1.stanowisko = p.stanowisko) AS K,
       (SELECT COUNT(p1.plec)
        FROM personel p1
        WHERE p1.plec = 'M' AND p1.stanowisko = p.stanowisko) AS M
    FROM personel p;
GO

-- Wyświetl numery wszystkich biur, które nie oferują żadnych nieruchomości.
-- 11
SELECT b.biuronr
    FROM biura b
    FULL OUTER JOIN nieruchomosci n on b.biuronr = n.biuronr
    GROUP BY b.biuronr
    HAVING COUNT(n.nieruchomoscnr) = 0;
GO

-- Wyświetl nazwy wszystkich miast, z których pochodzą klienci,
-- a w których swojej siedziby nie ma żadne biuro.
-- 12

SELECT TRIM(SUBSTRING(k.adres, 7, len(k.adres))) AS miasto
    FROM klienci k
    WHERE TRIM(SUBSTRING(k.adres, 7, len(k.adres))) NOT IN (
        SELECT DISTINCT b.miasto
        FROM biura b
        );
GO

-- Dla każdego biura wyświetl jego numer oraz łączny zysk
-- obliczony jako 30% pobranego czynszu. Zastosuj alias zysk.
-- 13
SELECT b.biuronr, SUM(n.czynsz)*0.3 AS zysk
    FROM biura b
    LEFT JOIN nieruchomosci n on b.biuronr = n.biuronr
    GROUP BY b.biuronr;
GO

-- Dla każdej nieruchomości wyświetl jej numer, liczbę wizyt oraz liczbę wynajmów.
-- Zastosuj aliasy wizyty oraz wynajmy.
-- 14
SELECT n.nieruchomoscnr, (
    SELECT COUNT(w1.nieruchomoscnr)
        FROM wizyty w1
        WHERE w1.nieruchomoscnr = n.nieruchomoscnr
    ) AS wizyty,
    (SELECT COUNT(w2.nieruchomoscnr)
        FROM wynajecia w2
        WHERE w2.nieruchomoscnr = n.nieruchomoscnr
    ) AS wynajmy
    FROM nieruchomosci n;
GO

-- Dla każdej nieruchomości wyświetl jej numer oraz wyrażoną w procentach
-- podwyżkę czynszu pomiędzy jego pierwotną a obecną wartością.
-- 15
SELECT n.nieruchomoscnr, (n.czynsz-(
    SELECT TOP 1 w.czynsz
        FROM wynajecia w
        WHERE w.nieruchomoscnr = n.nieruchomoscnr
        ORDER BY w.od_kiedy
    ))*100/n.czynsz AS podwyzka
    FROM nieruchomosci n
GO

-- Dla każdej nieruchomości wyświetl jej numer oraz łączną kwotę pobranego czynszu
-- 16
SELECT nier.nieruchomoscnr as numer_nieruchomiosci,
       wys.summa FROM nieruchomosci nier
JOIN (SELECT sum(wy.czynsz) as summa, wy.nieruchomoscnr FROM wynajecia wy
group by wy.nieruchomoscnr, wy.zaplacona having wy.zaplacona = 1) wys on wys.nieruchomoscnr = nier.nieruchomoscnr;
GO

-- Wyświetl numer najczęściej oglądanej nieruchomości. Uwzględnij, że rezultatów może być kilka.
-- 17
SELECT DISTINCT wiz.nieruchomoscnr as numer_nieruchomosci
FROM wizyty wiz,
    (SELECT TOP 1 wiz1.nieruchomoscnr, count(wiz1.nieruchomoscnr) as t FROM wizyty wiz1
    group by wiz1.nieruchomoscnr ORDER BY t DESC) f
WHERE wiz.nieruchomoscnr = f.nieruchomoscnr
GO

-- Wyświetl numer nieruchomości, która była wynajęta przez największą liczbę dni. Uwzględnij, że
-- rezultatów może być kilka.
-- 18
SELECT DISTINCT wyn.nieruchomoscnr as numer_nieruchomosci
FROM wynajecia wyn,
    (SELECT TOP 1 wyn1.nieruchomoscnr as t FROM wynajecia wyn1
    group by wyn1.nieruchomoscnr ORDER BY max(datediff(day, wyn1.od_kiedy, wyn1.do_kiedy)) DESC) f
WHERE wyn.nieruchomoscnr = f.t
GO

-- Dla każdego wynajmu wyświetl numer umowy oraz wyrażony w dniach czas jego trwania.
-- 19
SELECT DISTINCT wyn.umowanr, datediff(day, wyn.od_kiedy, wyn.do_kiedy) as czas_trwania
FROM wynajecia wyn;
GO

-- Dla każdej formy płatności wyświetl jej nazwę oraz liczbę umów, zgodnie z którymi czynsz jest
-- opłacany z jej wykorzystaniem.
-- 20
SELECT wyn.forma_platnosci, count(wyn.forma_platnosci)
FROM wynajecia wyn group by wyn.forma_platnosci, wyn.zaplacona HAVING wyn.zaplacona = 1;


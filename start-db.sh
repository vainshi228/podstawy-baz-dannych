#!/bin/bash

cd "${0%/*}" || exit
set -e

mkdir -p ./tmp
mkdir -p ./tmp/data

podman-compose up -d
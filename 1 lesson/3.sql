CREATE TABLE KLIENCI (
    client_id INT PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    payment_realize BOOL,
    payment_date DATE
);
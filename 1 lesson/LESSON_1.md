# Zadanie laboratoryjne 1

## Zadanie laboratoryjne 1 – Podstawy baz danych

## Ćwiczenie 1

Poniższe informacje dotyczą prawa jazdy. Zidentyfikuj, które z nich stanowią dane, a które metadane. Określ typy (dziedzinę) danych.

- Imię i nazwisko kierowcy.
- Adres.
- Data urodzenia.
- Informacja, że nazwisko kierowcy składa się z maksymalnie 30 znaków.
- Zdjęcie kierowcy.
- Zdjęcie odcisków palców kierowcy.
- Marka i numer seryjny urządzenia skanującego, które zostało użyte do zeskanowania odcisków linii papilarnych.
- Rozdzielczość (w megapikselach) aparatu, który był używany do fotografowania kierowcy.
- Informacja, że data urodzenia kierowcy musi poprzedzać dzisiejszą datę o co najmniej 18 lat.

## Ćwiczenie 2

Opisz wybraną, znaną Ci bazę danych – każdy z podpunktów rozwiń w kilku zdaniach.

- Krótko wyjaśnij, jaki obszar wsparcia zapewnia wybrana przez Ciebie baza danych (zarządzanie zamówieniami, biblioteka, system bankowości osobistej).
- Jak Twoim zdaniem korzystanie z bazy danych wpływa na jakość usługi lub produktu dostarczanego przez tę organizację lub firmę? Odpowiedź uzasadnij.
- Wypisz typy encji (zbiory encji) i związki, które Twoim zdaniem zawiera baza danych. Jeśli nie jesteś pewien, co należy uwzględnić, najpierw zastanów się nad obiektami i ich danymi, które Twoim zdaniem będą musiały być przechowywane w bazie danych, a następnie utwórz typy encji z tych obiektów. Przykład: w bazie danych księgarni wybrane dane, które można wskazać to: cena, tytuł, autor, stan magazynowy. Są to atrybuty, które muszą być przechowywane w odniesieniu do określonego obiektu (rzeczy): książka (encja).
- Następnie spróbuj zidentyfikować związki, które łączą zbiory encji.

## Ćwiczenie 3

Załóżmy, że na potrzeby pewnej organizacji opracowana została baza danych, a w niej tabela KLIENCI ze wskazanymi poniżej polami. Zastanów się nad ich znaczeniem i możliwością zastosowania.

- Identyfikator klienta (pole numeryczne, automatycznie numerowane).
- Nazwa klienta (pole tekstowe).
- Realizacja płatności (pole typu logicznego).
- Data płatności (pole daty).

## Ćwiczenie 4

Załóżmy, że istnieje potrzeba opracowania bazy danych dla małego działu księgowego w firmie podatkowej. Zastanów się, jakie typy encji powinny zostać wyodrębnione?  Wymień je i wyjaśnij ich znaczenie. Zaprojektuj model danych w dowolnej formie, pozwalającej na dyskusję projektu. Wyjaśnij związki pomiędzy zbiorami encji.
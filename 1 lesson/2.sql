-- USE master;
-- GO
-- DROP DATABASE IF EXISTS Delivery;
-- GO
-- CREATE DATABASE Delivery;
-- GO
-- USE Delivery;
-- GO
-- DROP TABLE IF EXISTS clients;
-- GO
-- CREATE TABLE clients (
    -- client_id INT NOT NULL,
-- 
    -- name VARCHAR(255) NOT NULL,
    -- surname VARCHAR(255) NOT NULL,
-- 
    -- CONSTRAINT PK_clients PRIMARY KEY (client_id)
-- );
-- GO
-- DROP TABLE IF EXISTS orders;
-- GO
-- CREATE TABLE orders (
    -- order_id INT NOT NULL,
-- 
    -- date_of_delivery DATE NOT NULL,
    -- client_id INT NOT NULL,
-- 
    -- CONSTRAINT PK_orders PRIMARY KEY (order_id),
    -- CONSTRAINT FK_client
        -- FOREIGN KEY (client_id) REFERENCES clients (client_id)
-- );
-- GO
-- DROP TABLE IF EXISTS product;
-- GO
-- CREATE TABLE product (
    -- product_id INT NOT NULL,
-- 
    -- name VARCHAR(255) NOT NULL,
    -- CONSTRAINT PK_product PRIMARY KEY (product_id),
-- );
-- GO
-- DROP TABLE IF EXISTS product_order;
-- GO
-- CREATE TABLE product_order (
    -- product_id INT NOT NULL,
    -- order_id INT NOT NULL,
    -- CONSTRAINT PK_product_order PRIMARY KEY (product_id, order_id),
    -- CONSTRAINT FK_product
        -- FOREIGN KEY (product_id) REFERENCES product (product_id),
    -- CONSTRAINT FK_order
        -- FOREIGN KEY (order_id) REFERENCES orders (order_id)
-- );
-- GO
-- USE master;
-- GO
USE master;
GO
DROP DATABASE IF EXISTS Biblioteka;
GO
CREATE DATABASE Biblioteka;
GO
USE Biblioteka;
GO
DROP TABLE IF EXISTS Autor;
GO
CREATE TABLE Autor (
Autor_id INT NOT NULL,
CONSTRAINT PK_Autor PRIMARY KEY (Autor_id)
);
GO
DROP TABLE IF EXISTS Wypozyczenie;
GO
CREATE TABLE Wypozyczenie (
Wypozyczenie_id INT NOT NULL,
Czytelnik_id INT NOT NULL,
Pracownik_id INT NOT NULL,
CONSTRAINT PK_Wypozyczenie PRIMARY KEY (Wypozyczenie_id),
CONSTRAINT FK_Czytelnik
FOREIGN KEY (Czytelnik_id) REFERENCES Czytelnik (Czytelnik_id),
CONSTRAINT FK_Pracownik
FOREIGN KEY (Pracownik_id) REFERENCES Pracownik (Pracownik_id)
);
GO
DROP TABLE IF EXISTS Czytelnik;
GO
CREATE TABLE Czytelnik (
Czytelnik_id INT NOT NULL,
CONSTRAINT PK_Czytelnik PRIMARY KEY (Czytelnik_id)
);
GO
DROP TABLE IF EXISTS Pracownik;
GO
CREATE TABLE Pracownik (
Pracownik_id INT NOT NULL,
CONSTRAINT PK_Pracownik PRIMARY KEY (Pracownik_id)
);
GO
Anastasiya Zalataya, Piotr Tomczyk, Krzysztof Stawinoga, Yauheni Nestsiaruk
DROP TABLE IF EXISTS Wydawca;
GO
CREATE TABLE Wydawca (
Wydawca_id INT NOT NULL,
CONSTRAINT PK_Wydawca PRIMARY KEY (Wydawca_id)
);
GO
DROP TABLE IF EXISTS Ksiazka;
GO
CREATE TABLE Ksiazka (
Ksiazka_id INT NOT NULL,
Autor_id INT NOT NULL,
Wydawca_id INT NOT NULL,
CONSTRAINT PK_Ksiazka PRIMARY KEY (Ksiazka_id),
CONSTRAINT FK_Wydawca
FOREIGN KEY (Wydawca_id) REFERENCES Wydawca (Wydawca_id),
CONSTRAINT FK_Autor
FOREIGN KEY (Autor_id) REFERENCES Autor (Autor_id)
);
GO
USE master;
GO

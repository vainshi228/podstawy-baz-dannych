-- USE master;
-- GO
-- DROP DATABASE IF EXISTS TaxCompany;
-- GO
-- CREATE DATABASE TaxCompany;
-- GO
-- USE TaxCompany;
-- GO
-- DROP TABLE IF EXISTS accountants;
-- GO
-- CREATE TABLE accountants (
--     accountant_id INT NOT NULL IDENTITY(1,1),
--
--     name VARCHAR(255) NOT NULL,
--     surname VARCHAR(255) NOT NULL,
--
--     worker_id VARCHAR(32) NOT NULL,
--
--     CONSTRAINT PK_accountants PRIMARY KEY (accountant_id)
-- );
-- GO
-- DROP TABLE IF EXISTS clients;
-- GO
-- CREATE TABLE clients (
--     client_id INT NOT NULL IDENTITY(1,1),
--
--     name VARCHAR(255) NOT NULL,
--     surname VARCHAR(255) NOT NULL,
--
--     CONSTRAINT PK_clients PRIMARY KEY (client_id)
-- );
-- GO
-- DROP TABLE IF EXISTS taxes;
-- GO
-- CREATE TABLE taxes (
--     tax_id INT NOT NULL IDENTITY(1,1),
--
--     date_of_tax DATE NOT NULL,
--     tax_count MONEY NOT NULL,
--
--     accountant_id INT NOT NULL,
--     client_id INT NOT NULL,
--
--     CONSTRAINT PK_taxes PRIMARY KEY (tax_id),
--     CONSTRAINT FK_accountant
--         FOREIGN KEY (accountant_id) REFERENCES accountants (accountant_id),
--     CONSTRAINT FK_client
--         FOREIGN KEY (client_id) REFERENCES clients (client_id)
-- );
-- GO
-- USE master;
-- GO
USE master;
GO
DROP DATABASE IF EXISTS TaxCompany1;
GO
CREATE DATABASE TaxCompany1;
GO
USE TaxCompany1;
GO
DROP TABLE IF EXISTS Klient;
GO
CREATE TABLE Klient (
    klient_id int not null IDENTITY(1,1),
    imie varchar(25) not null,
    nazwisko varchar(30) not null,
    numer_telefonu varchar(13),
    adres text,
    constraint PK_KLIENT_ID primary key (klient_id),
);
GO
DROP TABLE IF EXISTS Pracownik;
GO
CREATE TABLE Pracownik (
    pracownik_id int not null IDENTITY(1,1),
    imie varchar(25) not null,
    nazwisko varchar(30) not null,
    numer_telefonu varchar(13),

    constraint PK_Pracownik primary key (pracownik_id),
);
GO
DROP TABLE IF EXISTS Podatek;
GO
CREATE TABLE Podatek (
    podatek_id int not null IDENTITY(1,1),
    suma_do_zaplaty money not null,
    suma_zaplacona money not null,
    klient_id int not null,

    constraint PK_Podatek primary key (podatek_id),
    constraint FK_KLIENT_PODATEK foreign key (klient_id)
                     references Klient(klient_id)
);
GO
DROP TABLE IF EXISTS Zlecenie;
GO
CREATE TABLE Zlecenie (
    zlecenie_id int not null IDENTITY(1,1),
    data_zalecenia date default getdate(),
    wartosc money not null,
    pracownik_id int not null,
    podatek_id int not null,
    constraint PK_Zlecenie primary key (zlecenie_id),

    constraint FK_Z_pracownik foreign key (pracownik_id)
        references Pracownik(pracownik_id),
    constraint FK_Z_podatek foreign key (podatek_id)
        references Podatek(podatek_id),
);
GO
DROP TABLE IF EXISTS ZleceniePracownik;
GO
CREATE TABLE ZleceniePracownik (
    zlecenie_id int not null,
    pracownik_id int not null,
    constraint PK_ZleceniePracownik primary key (zlecenie_id, pracownik_id),
    constraint FK_ZP_zlecenie foreign key (zlecenie_id)
        references Zlecenie(zlecenie_id),
    constraint FK_ZP_pracownik foreign key (pracownik_id)
        references Pracownik(pracownik_id),
);
GO
USE master;
GO
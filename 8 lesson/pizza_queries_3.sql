USE PIZZA;

-- 1.1
SELECT country, avg(price) as avg_price FROM MENU
GROUP BY country
HAVING country IS NOT NULL;

-- 1.2
SELECT country, max(price) as max_price FROM MENU
GROUP BY country
HAVING country IS NOT NULL;

-- 1.3
SELECT country, min(price) as min_price FROM MENU
GROUP BY country
HAVING country IS NOT NULL;

-- 1.4
SELECT country, avg(price) as avg_price FROM MENU
GROUP BY country
HAVING count(pizza) > 1
AND country IS NOT NULL;

-- 1.5
SELECT country, min(price) as avg_price FROM MENU
GROUP BY country
HAVING country LIKE '%i%'
AND country IS NOT NULL;

-- 1.6
SELECT country, min(price) as min_price FROM MENU
GROUP BY country
HAVING min(price) < 7.50
AND country IS NOT NULL;

--
-- 2
--

-- 2.1
SELECT pizza, price FROM MENU m1
WHERE m1.price > all (SELECT price FROM MENU m2 WHERE m2.country = 'Italy')

-- 2.2
SELECT DISTINCT pizza FROM RECIPES r1
WHERE r1.ingredient = any (SELECT i1.ingredient FROM ITEMS i1 WHERE type = 'meat')

-- 2.3
SELECT r1.ingredient, r1.pizza, r1.amount FROM RECIPES r1
WHERE pizza IN (SELECT pizza FROM RECIPES WHERE ingredient = r1.ingredient
                                            AND amount = (SELECT MAX(amount) FROM RECIPES WHERE ingredient = r1.ingredient))
ORDER BY ingredient DESC;

-- 2.4
SELECT DISTINCT r1.ingredient FROM RECIPES r1
WHERE r1.ingredient IN (SELECT ingredient FROM RECIPES GROUP BY ingredient HAVING COUNT(*) > 1);

-- 2.5
SELECT pizza FROM MENU
WHERE pizza <> 'siciliano' AND country = (SELECT country FROM MENU WHERE pizza = 'siciliano');

-- 2.6
SELECT ingredient from ITEMS
WHERE ingredient NOT IN (SELECT DISTINCT ingredient from RECIPES);

-- 2.7
SELECT DISTINCT r1.ingredient FROM RECIPES r1
WHERE 1 < (SELECT COUNT(*) FROM RECIPES r2 WHERE r2.ingredient = r1.ingredient);

-- 2.8
SELECT m1.pizza, m1.price FROM MENU m1
WHERE m1.price < (SELECT m2.price FROM MENU m2 WHERE pizza = 'napoletana')
  AND m1.price > (SELECT m2.price FROM MENU m2 WHERE pizza = 'garlic');

-- 2.9
SELECT DISTINCT r1.pizza FROM RECIPES r1,
(SELECT TOP 1 COUNT(r2.ingredient) as t,
              r2.pizza as name FROM RECIPES r2
GROUP BY r2.pizza ORDER BY t DESC) r3
WHERE r1.pizza = r3.name;

-- 2.10
SELECT DISTINCT i1.type FROM ITEMS i1 WHERE i1.ingredient IN
    (SELECT r1.ingredient FROM RECIPES as r1 WHERE r1.pizza =
    (SELECT m1.pizza as name FROM MENU m1 WHERE (SELECT MAX(m2.price) FROM MENU m2) = m1.price));

USE master;
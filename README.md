### Podstawy baz dannych

Uruchomienie bazy
```sh
./start-db.sh # Linux
```
Wyłączenie bazy
```sh
./stop-db.sh # Linux
```
Podłączenie:
- host: `localhost` lub `127.0.0.1`
- port: `5441`
- username: `sa`
- password: `_Password12`

Inicjalizacja bazy szpital
- [skrypt inicjalizacji]("./2 lesson/0.sql")
- [skrypt dodania tabel Karty miedyczne i Preciptions]("./2 lesson/1.sql")
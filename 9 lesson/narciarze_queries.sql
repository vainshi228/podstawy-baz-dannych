use narciarze;
go
-- 1

select z.imie, z.nazwisko, datediff(year, z.data_ur, GETDATE()) as wiek
from zawodnicy z order by wiek desc, z.nazwisko;
go
-- 2

SeLeCt z.imie, z.nazwisko, datediff(year, z.data_ur, z2.data) as wiek
from zawodnicy z
LEFT JOIN (SELECT DISTINCT CTE.id_zawodow, CTE.id_skoczka
 FROM
    (SELECT *, ROW_NUMBER() OVER(PARTITION BY id_skoczka ORDER BY id_zawodow) Corr
     FROM uczestnictwa_w_zawodach) CTE
 WHERE CTE.Corr = 1
) as u on z.id_skoczka = u.id_skoczka
LEFT JOIN zawody z2 on u.id_zawodow = z2.id_zawodow
order by wiek desc, z.nazwisko;
go
-- 3

SELECT imie, nazwisko from zawodnicy
WHERE LEFT(imie, 1) = LEFT(nazwisko, 1);
go
-- 4

SELECT imie, nazwisko from zawodnicy
left join uczestnictwa_w_zawodach uwz on zawodnicy.id_skoczka = uwz.id_skoczka
where id_zawodow is null;
go
-- 5

select z.imie, z.nazwisko, count(uwz.id_zawodow) as 'liczba zawodow'
from zawodnicy z
JOIN uczestnictwa_w_zawodach uwz on z.id_skoczka = uwz.id_skoczka
GROUP BY z.imie, z.nazwisko;
go
-- 6

select z.imie, z.nazwisko, count(uwz.id_zawodow) as 'liczba zawodow'
from zawodnicy z
JOIN uczestnictwa_w_zawodach uwz on z.id_skoczka = uwz.id_skoczka
JOIN zawody z2 on uwz.id_zawodow = z2.id_zawodow
JOIN skocznie s on z2.id_skoczni = s.id_skoczni
GROUP BY z.imie, z.nazwisko, s.id_kraju, z.id_kraju HAVING s.id_kraju = z.id_kraju;
go
-- 7

select z.imie, z.nazwisko, s.nazwa
from zawodnicy z
JOIN uczestnictwa_w_zawodach uwz on z.id_skoczka = uwz.id_skoczka
JOIN zawody z2 on uwz.id_zawodow = z2.id_zawodow
JOIN skocznie s on z2.id_skoczni = s.id_skoczni
GROUP BY z.imie, z.nazwisko, s.nazwa
go
-- 8

select nazwa, (sedz - k) as odleglosc from skocznie;
go
-- 9

select nazwa, max(sedz) as max_dl from skocznie
JOIN zawody z on skocznie.id_skoczni = z.id_skoczni
WHERE id_zawodow is not null
GROUP BY nazwa, sedz HAVING max(sedz) = (SELECT max(d.sedz) FROM skocznie d)
ORDER BY max_dl desc;
go
-- 10

select k.kraj, count(s.id_skoczka) FROM kraje k
LEFT JOIN zawodnicy s on k.id_kraju = s.id_kraju
GROUP BY k.kraj;
go
-- 11

select k.kraj FROM kraje k
LEFT JOIN zawodnicy s on k.id_kraju = s.id_kraju
GROUP BY k.kraj HAVING count(s.id_skoczka) = 0;
go
-- 12

select k.kraj FROM kraje k
LEFT JOIN skocznie s on k.id_kraju = s.id_kraju
GROUP BY k.kraj HAVING count(s.id_skoczni) = 0;
go
-- 13

select k.kraj FROM kraje k
LEFT JOIN skocznie s on k.id_kraju = s.id_kraju
LEFT JOIN zawody z on z.id_skoczni = s.id_skoczni
GROUP BY k.kraj HAVING count(z.id_zawodow) > 0;
go
-- 14

select k.kraj FROM kraje k
LEFT JOIN skocznie s on k.id_kraju = s.id_kraju
LEFT JOIN zawody z on z.id_skoczni = s.id_skoczni
GROUP BY k.kraj HAVING count(z.id_zawodow) = (
    SELECT TOP 1 count(z2.id_zawodow) as hy FROM zawody z2
    JOIN skocznie s2 on s2.id_skoczni = z2.id_skoczni
    RIGHT JOIN kraje k2 on k2.id_kraju = s2.id_kraju
    GROUP BY k2.id_kraju
    ORDER BY hy desc
    );
go
-- 15

SELECT (datename(weekday, z.data)) as dzienTygodnia
from zawody z
where z.data = (select min(z2.data) from zawody z2)
or z.data = (select max(z2.data) from zawody z2)
go
-- 16
insert into trenerzy (nazwisko_t, imie_t, data_ur_t, id_kraju)
VALUES (
        (select top 1 value from string_split('Corby Fisher', ' ') order by value desc),
        (select top 1 value from string_split('Corby Fisher', ' ')),
        Convert(smalldatetime, '20.07.1975', 104),
        (SELECT id_kraju from kraje WHERE kraj = 'USA'));
go
SELECT * FROM trenerzy WHERE nazwisko_t = 'Fisher';
go

-- 17

ALTER TABLE zawodnicy
ADD id_trenera int;
go
ALTER TABLE zawodnicy
ADD CONSTRAINT fk_trener
FOREIGN KEY (id_trenera) REFERENCES trenerzy(id_trenera);
SELECT TOP 1 * FROM zawodnicy;
go

-- 18
UPDATE zawodnicy
    SET zawodnicy.id_trenera = t.id_trenera
    FROM zawodnicy z
    INNER JOIN trenerzy t ON z.id_kraju = t.id_kraju;
go
SELECT * FROM zawodnicy;
go

-- 19
SELECT * FROM trenerzy WHERE data_ur_t IS NULL;
UPDATE
    trenerzy
SET
    data_ur_t = DATEADD(year, -5, (SELECT TOP 1 MIN(z.data_ur) FROM zawodnicy z
        JOIN trenerzy t ON z.id_trenera=t.id_trenera
        GROUP BY t.id_trenera))
WHERE data_ur_t IS NULL;
SELECT * FROM trenerzy;
go

-- 20
SELECT t.imie_t, t.nazwisko_t, count(z.id_skoczka) as liczbaZawodnikow
    from trenerzy t
    join zawodnicy z on t.id_trenera = z.id_trenera
    group by imie_t, nazwisko_t having count(z.id_skoczka) > 1;
go
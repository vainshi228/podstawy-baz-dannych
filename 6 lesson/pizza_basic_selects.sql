USE PIZZA;

SELECT * FROM MENU ORDER BY pizza; --(1)

SELECT * FROM MENU ORDER BY price DESC, pizza ASC; --(2)

SELECT DISTINCT (price) FROM MENU ORDER BY price; --(3)

SELECT pizza, price, country, base --(4)
FROM MENU
WHERE country = 'Italy'
  AND price < 7;

SELECT * FROM MENU WHERE country NOT IN('U.S.', 'Italy') OR country is NULL; --(5)

SELECT * FROM MENU WHERE pizza IN ('americano', 'garlic', 'mexicano', 'vegetarian'); --(6)

SELECT * FROM MENU WHERE price < 7 AND price > 6; --(7)

SELECT * FROM MENU WHERE pizza LIKE '%ano'; --(8)

SELECT * FROM MENU WHERE country IS NOT NULL; --(9)

SELECT DISTINCT amount FROM RECIPES WHERE ingredient = 'spice' ORDER BY amount ASC; --(10)

USE master;
GO
